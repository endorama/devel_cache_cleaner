CONTENTS OF THIS FILE
---------------------
  
  * Introduction
  * Installation
  * Contributors


INTRODUCTION
------------

This module simply add the php code needed to clear all caches at every page
load. Enable it and start developing your modules much more quicker! No need to
manually rebuild/clean caches!

Also it enables PHP error output, useful when developing modules, which avoid
( in most cases ) the Drupal White Screen Of Death!

NB: Remember to disable it after developing!


INSTALLATION
------------

1.  Download the module

2.  Install & enable the module. After that the caches are going to be rebuild
    on every page refresh.


CONTRIBUTORS
------------
  * endorama ( Edoardo Tenani )
